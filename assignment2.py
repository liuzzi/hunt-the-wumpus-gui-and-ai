#!/usr/bin/env python
#Francesco Liuzzi
#Artificial Intelligence
#Logic Programming Assignment

import numpy
import sys
import subprocess
import Image
from Tkinter import *
from ttk import Frame, Button, Label, Style
from ttk import Entry

class PROVER9KB:
#very basic set-based knowledge-base that uses prover9 to deduce bad moves

    def __init__(self, agent):
        self.agent = agent
        self.generateAssumptions((agent.rows,agent.cols))
        self.clauses = set()
        self.last_proof = False

    #writes  .grid_assumptions for every square
    def generateAssumptions(self,size_of_map):
        rows = size_of_map[0]
        cols = size_of_map[1]
        writer = open(".grid_assumptions","w")

        first_part = "all X all Y (adjacent("
        second_part = ",X,Y) <-> "

        for x in range(0,rows+1):
            for y in range(0,cols+1):
                #for every coordinate, generate adjacency logic 
                line = first_part+str(x)+','+str(y)+second_part
                adjacents = self.agent.getValidMoves(x, y)
                for coord in adjacents:
                    u = coord[0]
                    v = coord[1]
                    line += '(X='+str(u)+'&Y='+str(v)+')|'
                
                #strip extra or at the end
                line = line[:-1]
                writer.write(line+').\n')
        writer.close()
        print "Generated grid logic assumptions."

  
    def ask(self,position,prove):
     # +Write to file:
     #   - generate header
     #   - insert clauses
     #   - end of line
     #   - generate goals
     # +run prover9, check output for 'THEOREM PROVED'


        #write header
        f = open("prover9header")
        lines = f.readlines()
        f.close()

        #write header
        writer = open("input.in","w")
        for line in lines:
            writer.write(line)


        #write grid assumptions

        f = open(".grid_assumptions")
        lines = f.readlines()
        f.close()
        
        for line in lines:
            writer.write(line)

        
        writer.write("end_of_list.\n")
        writer.write("formulas(sos).\n")

        #write clauses
        for cl in self.clauses:
            writer.write(cl+"\n")

        #end of list
        end_of_list = "end_of_list.\n"
        writer.write(end_of_list)

        #goals
        position_string = prove+str(position)
        goals = "\nformulas(goals).\n" + position_string + ".\nend_of_list.\n"

        writer.write(goals)
        writer.close()
        #done writing file, run prover9.
        p = subprocess.Popen('./bin/prover9 -f input.in > out.out', 
                             shell=True,stdout=subprocess.PIPE,
                             stdin=subprocess.PIPE, 
                             stderr=subprocess.PIPE)
        p.wait()

        #scan output from prover9
        f = open("out.out")
        lines = f.readlines()
        f.close()

        found = False
        for line in lines:
            if "THEOREM PROVED" in line:
                found = True
                self.last_proof = prove
                return True

        if not found:
            print "Could not deduce " + str(prove) + " at " + str(position)
            prove = None
        
        return prove


    def tell(self,currentPos,visited,moves,breeze,stench):
        x = currentPos[0]
        y = currentPos[1]
        dot = '.'

        if not breeze and not stench:
            print "safe at "+str(currentPos)
            self.clauses.add("safe"+str(currentPos)+dot)
            return

        if breeze:
            print "breeze at "+str(currentPos)
            self.clauses.add("breezy"+str(currentPos)+dot)

        else:
            self.clauses.add("-breezy"+str(currentPos)+dot)
            
        if stench:
            print "stench at "+str(currentPos)
            self.clauses.add("smelly"+str(currentPos)+dot)
        else:          
            self.clauses.add("-smelly"+str(currentPos)+dot)


        

class AGENT:
#   instance vars:
#   ----------------------------------
#   map = numpy matrix of lists
#   pot = num gold in map (originally)
#   currentPos = current agent position
#   goalPos = goal position
#   rows = rows in map - 1
#   cols = cols in map - 1
#   visited = list of all visited coords
#   gold_collected = current gold collected by agent
#   escape = boolean telling if the agent collected all gold and should escape
#   seen_goal = whether the agent has come across the goal square
#   wumpus_alive = 
#   arrows = starting arrow count (as of now game only supports one wumpus so one arrow)
#   escape_index = index in the visited list where the escape begins
#   score = the agent's score
#   move_num = the number of moves the agent has taken

    

    def __init__(self, map_txt):
        self.parseFile(map_txt)
        self.kb = PROVER9KB(self)
        self.known_pits = set()
        self.known_wumpus = set()
        self.gold_collected = 0
        self.escape = False
	self.seen_goal = False
	self.wumpus_alive = True
	self.arrows = 1
        self.escape_index = 0
        self.score = 0
        self.move_num = 0        
        self.cells = (self.rows+1)*(self.cols+1)
        #scoring options
        self.MOVEPENALTY = -1
        self.ARROWPENALTY = -100
        self.GOLDMULTIPLIER = 1000

    def start(self):
        while not self.is_game_over():
            self.move()
            print self.map


    def is_game_over(self):
        current_cell = self.map.item(self.currentPos[0],self.currentPos[1])
        if 'GO' in current_cell and self.escape:
            print "SUCCESS! you escaped with " + str(self.gold_collected) + " gold!" 
            print "\nSCORE: "+str(self.score)
            return True
        if 'W' in current_cell:
            print "DIED. The wumpus killed you!"
            return True
        if 'P' in current_cell:
            print "DIED.  Fell in a pit."
            return True

        return False

    #gives the distance between two coordinates
    def getCoordDistance(self, coord1, coord2):
        return ((coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2)**.5
    

    #move one towards goalPos
    def makeEscapeMove(self):
            self.moveTo(self.visited[self.escape_index])
            self.escape_index -= 1


    def killWumpus(self):
        #remove stenches
 	#remove wumpus 
	print "KILLING THE WUMPUS!"
	for x in range(0,self.rows+1):
		for y in range(0,self.cols+1):
        		cell = self.map.item(x,y)
			if 'S' in cell:
				cell.remove('S')
                                #add dead stench
                                cell.add('DS')
			if 'W' in cell:
				cell.remove('W')
                                #dead wumpus (for GUI)
                                cell.add('DW')
       
	print "Wumpus and stenches removed from map."
        #set flag that wumpus is dead
        self.wumpus_alive = False
	self.arrows -= 1
        #take arrow penalty from score
        self.score += self.ARROWPENALTY

    def nextEscapeMove(self):
        self.move

    def move(self):
        if self.escape and self.seen_goal:
            self.makeEscapeMove()
            return
            
        moves = self.getValidMoves(self.currentPos[0],self.currentPos[1])
        breeze = False
        stench = False
       
        foundSafe = False
        current_cell = self.map.item(self.currentPos[0],self.currentPos[1])

	if 'GO' in current_cell:
            self.seen_goal = True

        if 'B' in current_cell:
            breeze = True  
            #check if we know the position of this breeze's pit.
                      
        if 'S' in current_cell:
            stench = True

        #update kb with current cell knowledge
        self.kb.tell(self.currentPos,self.visited,moves,breeze,stench)
        
        #if the square has no breeze or stench:
        if not breeze and not stench:
            found_unvisited = False
            for coord in moves:
                if coord not in self.visited:
                    print "No breeze or stench in square, moving to unvisited: " + str(coord)
                    self.moveTo(coord)
                    found_unvisited = True
                    break
            if not found_unvisited:
                print "couldnt find an unvisited square, going back to " + str(moves[0])
                self.moveTo(moves[0])
            return
        
        for coord in moves:
            if coord not in self.visited:
                #ask KB determine safety of square
                #if safe, move, remove 'A' from set, check for gold,  and break,
                #if not, evaluate others not visited
                safe = self.kb.ask(coord,'safe')

                if safe:
                    print "Deduced safe! moving to it at " + str(coord)
                    self.moveTo(coord)
                    return

                pit = self.kb.ask(coord,'pit')
                wumpus = self.kb.ask(coord,'wumpus')

                if safe == True:
                    print "Deduced safe! at " + str(coord)
                if pit == True:
                    print "Deduced pit at "+str(coord)
                    self.known_pits.add(coord)
                    #remove 
                if wumpus == True:
                    print "Deduced wumpus at "+str(coord)

                    
                    #kill the wumpus if movenum > resistance 
                    self.killWumpus()
                    self.known_wumpus.add(coord)
                

                #if we can't deduce anything...
                #at this point there is either a breeze or stench in the current cell
                if pit == None and wumpus == None:
                    print "Cannot deduce anything when danger present. not moving to "+str(coord)
                    continue

        #if we cant find a safe square, go back one.
        if not foundSafe:
            print "Couldnt find safe square ahead. Backtracking"
            self.moveTo(self.visited[len(self.visited)-2])


    #returns boolean saying whether theres any gold left
    def lastOfGold(self):
        return self.gold_collected == self.pot            

    def moveTo(self, coord):
        #decrement the agents score by the move penalty
        self.score += self.MOVEPENALTY
        
        #increment self.move_num
        self.move_num += 1

        current_cell = self.map.item(self.currentPos[0],self.currentPos[1])
        current_cell.remove('A')
        self.currentPos = coord
        self.visited.append(coord)
        
        new_cell = self.map.item(coord)
        if 'G' in new_cell:
            print "Found Gold!"
            self.gold_collected += 1
            #increment score
            self.score += self.GOLDMULTIPLIER
            new_cell.remove('G')
            if self.lastOfGold():
                print "Thats the last of the gold! making escape!"
		self.escape_index = len(self.visited)-1
                self.escape = True
                
        new_cell.add('A')
        print "Moved to "+str(coord)

    def getValidMoves(self, x, y):
        #corners
        if x == 0 and y == 0:
            return ((1,0),(0,1))
        elif x == self.rows and y == self.cols:
            return ((self.rows-1,self.cols),(self.rows,self.cols-1))
        elif x == self.rows and y == 0:
            return ((self.rows-1,0),(self.rows,1))
        elif x == 0 and y == self.cols:
            return ((0,self.cols-1),(1,self.cols))
        #'middle' edges
        elif x == 0:
            return ((0,y-1),
                    (1,y),
                    (0,y+1))
        elif y == 0:
            return ((x-1,0),
                    (x,1),
                    (x+1,0))
        elif x == self.rows:
            return ((self.rows,y-1),
                    (self.rows-1,y),
                    (self.rows,y+1))
        elif y == self.cols:
            return ((x-1,self.cols),
                    (x+1,self.cols),
                    (x,self.cols-1))
        else: # free movement in all directions
            return ((x,y-1),
                    (x,y+1),
                    (x-1,y),
                    (x+1,y))

   
    def parseFile(self,map_txt):
        #load file lines
        f = open(map_txt)
        lines = f.readlines()
        f.close()
        #first find the dimension of the map
        loadedSizeDimensions = False
        for line in lines:
            if "M" in line:
                loadedSizeDimensions = True
                #initialize numpy matrix to empty sets
                self.rows = int(line[1])-1
                self.cols = int(line[2])-1
                self.map = numpy.empty((self.rows+1,self.cols+1),dtype=set)
                self.map.fill(set())
                for cell in numpy.nditer(self.map, flags=['refs_ok'],
                                      op_flags=['readwrite']):
                    cell[...] = set()

        if not loadedSizeDimensions:
            print "Error loading map dimensions!"
            sys.exit(0)
        
        #now fill matrix lists with attributes in file
        for line in lines:
            if "M" in line:
                continue
            elif "POT" in line:
                self.pot = int(line[3])
            elif "P" in line:     
                self.map.item((int(line[1])-1,int(line[2])-1)).add("P")
            elif "B" in line:
                self.map.item((int(line[1])-1,int(line[2])-1)).add("B")
            elif "GO" in line:
                self.goalPos = (int(line[2])-1,int(line[3])-1)
                self.map.item((int(line[2])-1,int(line[3])-1)).add("GO")
                goalKnown = True
            elif "G" in line:
                self.map.item((int(line[1])-1,int(line[2])-1)).add("G")
            elif "S" in line:
                self.map.item((int(line[1])-1,int(line[2])-1)).add("S")
            elif "W" in line:
                self.map.item((int(line[1])-1,int(line[2])-1)).add("W")
            elif "A" in line:
                self.currentPos = (int(line[1])-1,int(line[2])-1)
                self.visited = [(int(line[1])-1,int(line[2])-1)]
                self.map.item((int(line[1])-1,int(line[2])-1)).add("A")
                agentPlaced = True
            

        if not goalKnown or not agentPlaced:
            print "Error finding starting and goal position!"
            sys.exit(0)

        print "Map successfully loaded."

class GUI(Frame):
    def moveAgent(self):
        current_cell = self.agent.map.item(self.agent.currentPos[0],self.agent.currentPos[1])
        
        
        if 'GO' in current_cell and self.agent.escape:
            self.button.config(state=DISABLED)
            self.button.pack()
            self.pack()
        else:
            self.agent.move()
            self.drawGameGrid()

    def drawGameGrid(self):
        wumpus_img = PhotoImage(file="images/wumpus.gif")
        dead_wumpus_img = PhotoImage(file="images/dead_wumpus.gif")
        pit_img = PhotoImage(file="images/pit.gif")
        stench_img = PhotoImage(file="images/stench.gif")
        breeze_img = PhotoImage(file="images/breeze.gif")
        stench_breeze_img = PhotoImage(file="images/stench_breeze.gif")
        agent_img = PhotoImage(file="images/agent.gif")
        goal_img = PhotoImage(file="images/goal.gif")
        gold_img = PhotoImage(file="images/gold.gif")
        blank_img = PhotoImage(file="images/blank.gif")

        padding = 64

        for y in range(0,self.agent.rows+2):
		for x in range(0,self.agent.cols+1):
                    #draw info pane at bottom
                    if y > self.agent.rows:
                        self.canvas.create_image(x*padding,y*padding+10, anchor=NW, image=blank_img)
                        self.blank_img = blank_img
                        score_text = self.canvas.create_text(self.canvas_width/2,y*padding+22, text="Score: "+str(self.agent.score), fill="black",font=("Helvectica", "11")) 
                        step_text = self.canvas.create_text(self.canvas_width/2,y*padding+38, text="Step: "+str(len(self.agent.visited)-1), fill="black",font=("Helvectica", "11"))
                        arrows_text = self.canvas.create_text(self.canvas_width/2,y*padding+54, text="Arrows: "+str(self.agent.arrows), fill="black",font=("Helvectica", "11"))
                        continue

                    cell = self.agent.map.item(y,x)
                    photo = ""
                    if 'W' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=wumpus_img)
                        self.wumpus_img = wumpus_img
                    elif 'DW' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=dead_wumpus_img)
                        self.dead_wumpus_img = dead_wumpus_img
                    elif 'P' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=pit_img)
                        self.pit_img = pit_img
                    elif 'B' in cell and not 'S' in cell and not 'DS' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=breeze_img)
                        self.breeze_img = breeze_img
                    elif ('S' in cell or 'DS' in cell) and 'B' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=stench_breeze_img)
                        self.stench_breeze_img = stench_breeze_img
                    elif 'S' in cell or 'DS' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=stench_img)
                        self.stench_img = stench_img
                    else:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=blank_img)
                        self.blank_img = blank_img
                        
                        #transparent objects
                    if 'A' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=agent_img)
                        self.agent_img = agent_img
                    if 'G' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=gold_img)
                        self.gold_img = gold_img
                    if 'GO' in cell:
                        self.canvas.create_image(x*padding,y*padding, anchor=NW, image=goal_img)
                        self.goal_img = goal_img

        

    def __init__(self, parent, agent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.agent = agent

        self.parent.title("Hunt The Wumpus - F. Liuzzi")

        self.button = Button(parent, text="Step Agent", command=self.moveAgent)
        self.button.pack()
        
        
        self.canvas_height = 64 * (self.agent.rows+1) + 64

        self.canvas_width = 64 * (self.agent.cols+1)
        #if self.agent.rows == self.agent.cols:
        #    self.canvas_height += 64
        self.canvas = Canvas(parent, width=self.canvas_width, height=self.canvas_height)
        #traverse agent map to display appropriate pictures
        

        self.drawGameGrid()
        
        self.canvas.pack()

        self.pack()

        
        
        


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print "ERROR:  Missing arguments:   map_file_path"
        sys.exit(0)

    #take map file path from command line args
    map_file = sys.argv[1]

    if '.txt' not in map_file:
        print "ERROR:  Unsupported file type or file not found: " + map_file
        sys.exit(0)


    agent = AGENT(map_file)
    print agent.map
    print "------------------------------------------------------"
    root = Tk()
    app = GUI(root,agent)
    root.mainloop()





